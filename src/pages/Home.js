// acquire all the components taht will make up the home page. (hero section, highlights)
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

// Let's create a data object that will describe the content of the Hero/Banner section
const details = {
	title: 'Welcome to the Home Page',
	content: 'Opportunities for everyone, everywhere'
}

// identify first how you want the landing page to be structured
export default function Home() {
	return(
    	// When wrapping multiple adjacent KSX elements
      	// <div> element tagging
			// <div>
			// 	<Banner />
			// 	<Highlights />
			// </div>
		// React Fragment - Group elements
		// for long version call import {Fragment} from 'react' and then user <Fragment></Fragment> to contain the JSX elements
		// shothand version is <> </> and does not need to call the import
		<>
			<Banner bannerData={details} />
			<Highlights />
		</>
	);
};