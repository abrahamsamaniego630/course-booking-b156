// Identify which component will be displayed
import { useState, useEffect }from 'react';
import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard';
import {Container} from 'react-bootstrap';

const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of courses'
}

// Catalog all Active courses
	// 1. Need Container Component for each course that we will retrieve from the database
	// 2. Declare a state for the Courses Collection, by default it is an empty array
	// 3. Create a side effect that will send a request to our backend API project.
	// 4. Pass down the retrieved resources inside the component as props.

export default function Courses() {

	// this array/storage will be used to save our course components for all active courses.
	
	const [coursesCollection, setCoursesCollection] = useState([]);

	// create an effect using our Effect Hook, provide a list of dependencies.
	// the effect that we are going to create is fetching data from the server.
	useEffect(() => {
		// fetch() => is a JS method which allows us to pass/create a request to an API.
		// SYNTAX: fetch(<request URL>, {OPTIONS})

		// A promise has 3 states: (Fulfilled, Reject, Pending)
		// We need to be able to handle the outcome of the promise.
		// We need to make the response usable on the frontend side. we need to convert it to a JSON format
		// Upon converting a fetch data to a JSON format a new promise will be executed
		fetch('https://stark-reef-18545.herokuapp.com/courses/')
		.then(res => res.json())
		.then(convertedData => {
			// we want the resources to be displayed in the page
			// since the data is stored in an array storage, we will iterate the array to explicitly get each document one by one
			// there will be multiple course card component that will be rendered that corresponds to each document
			// retrieved from the database. we need to utilize a storage to contain the cards that will be rendered
			setCoursesCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			})) 
		});
	},[]);
	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				{coursesCollection}	
			</Container>
		</>
	)
}