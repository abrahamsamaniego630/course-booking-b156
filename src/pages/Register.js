// identify the components needed to create the register page
import { useState, useEffect, useContext } from 'react';
import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const data = {
	title: 'Welcome to the Register Page',
	content: 'Create an account to enroll'
}

// declare a state for the input fields in the register page
// create a side effect on the page to render the button component if the components are not yet met

export default function Register() {

	// destructure the data from the context object for the user.
	const { user } = useContext(UserContext); // this is an object property{}

	// declare a state for the following components to make sure that they will be empty from the start
	// once you passed down the variables into their respective components they will be bounded to its value
	// for us to manage the 'states' of our components we will need a 'Hook' in React that will allow us to
	// handle state changes within the component
	// syntax: [getter, setter] = useState(value)
	// naming convention for state setters: set+variable
	// ATM we applied/created a 2 way binding in our form component, this 2-way binding will allow us to create

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState(''); //for password confirmation

	// declare a state for the button component, attack a state 'hook' to eventually manage and change the state of the button
	// to eventually change the state of the button, insert a state setter
	// now using the state hook, apply a conditional rendering to our Button component
	// naming convention for boolean values
		// is (prefix) + verb
		// => YES or NO / True or False
	const [isActive, setIsActive] = useState(false);
	// make a reactive response section for the password using conditional rendering
	const [isMatched, setIsMatched] = useState(false);

	const [isValid, setIsValid] = useState(false);
	// state for the heading section of the form
	const [isAllowed, setIsAllowed] = useState(false);

	// describe the side effects/effects that will react on how the user will interact with the form
	// to remove the warnings, we will provide the dependencies list
	useEffect(() => {

		// validate the data from the registration form to make sure that all inputs are clean before trying to perform a register function
		// NEW TASK: Make the reactions swift

		// we will modify the control structure to best suit our set rules bound by the register page. (LONG VERSION)
		if (
			// check if mobile is valid
			// mobile number digits should be 11
			mobileNo.length === 11
			) 
			// inform the user to give a better UX
		{
			setIsValid(true);
			if (
				//password checker
				(password1 === password2) && (password1 !== '' && password2 !== '')
			) {
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && email !== '') {
					setIsActive(true);
					setIsAllowed(true);
				} else {
					setIsActive(false);
					setIsAllowed(false);
				}
			} else {
				setIsActive(false);
				setIsMatched(false);
				setIsAllowed(false);
			}
		} else if (password1 !== '' && password1 === password2){
			setIsMatched(true);
		} else {
			// not permitted
			setIsActive(false);
			setIsMatched(false);
			setIsValid(false);
			setIsAllowed(false);
		};
	},[firstName, lastName, email, mobileNo, password1, password2]);
	// the Effect hook 'useEffect()' will use the dependencies list in order to identify which components to watch out for changes.
	// to avoid continuous form submission for every render of each component.

	// catch the 'click' event that will happen on the button component
	// we can make it as async to make sure that the response will be rendered out first
	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()
		// console.log(firstName)
		// console.log(lastName)
		// console.log(email)
		// console.log(mobileNo)
		// console.log(password1)
		// console.log(password2)

		// we will now modify this function so that we will be able to send a request to our API so that the
		// client will be able to create his own account

		// include an await expression in the fetch() to make sure that react will finish the task first before proceeding to the next
		const isRegistered = await fetch('https://stark-reef-18545.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    firstName: firstName,
			    lastName: lastName,
			    email: email,
			    password: password1,
			    mobileNo: mobileNo
			})
		}).then(response => response.json())
		.then(data => {
			// console.log(data);
			// create a control structure if success or fail
			// check if the email has data
			if (data.email) {
				return true;
			} else {
				return false;
			}
		})
		if (isRegistered) {
			// reset the registration form
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');

			// display a message that will confirm to the user that registration is successful.
			await Swal.fire({
				icon: 'success',
				title: 'Registration Successful',
				text: 'Thank you for creating an account.' 
			});
			// redirect the user to the login page for authentication
			window.location.href = "/login";

		} else {
			// response if registration has failed
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			});
		}
	};

	return(
		user.id
		?
			<Navigate to="/courses" replace={true} />
		:
		<>
			<Hero bannerData={data} />
			<Container className="container__box">
				{/*Form Heading*/}
				{
					isAllowed ?
						<h1 className="text-center text-success">You May Now Register</h1>
					:
						<h1 className="text-center">Register Form</h1>
				}
				<h6 className="mt-3 text-center text-secondary">Fill Up the Form Below</h6>

				<Form onSubmit={e => registerUser(e)}>
					{/*First Name Field*/}
					<Form.Group>
						<Form.Label>First Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your first name" value={firstName} onChange={event => setFirstName(event.target.value)} required />
					</Form.Group>
					{/*Last Name Field*/}
					<Form.Group>
						<Form.Label>Last Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your last name" value={lastName} onChange={event => setLastName(event.target.value)} required />
					</Form.Group>
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={event => setEmail(event.target.value)} required />
					</Form.Group>
					{/*Mobile Number Field*/}
					{/*For Capstone 3: customize this component so that you will get the correct format for the mobile number*/}
					<Form.Group>
						<Form.Label>Mobile No.: </Form.Label>
						<Form.Control type="number" placeholder="Enter your mobile no. here" value={mobileNo} onChange={event => setMobileNo(event.target.value)} required />
							{
								isValid ?
									<span className="text-success">Mobile Number is Valid!</span>
								:
									<span className="text-danger">Mobile Number Should Be 11-digits!</span>
							}
					</Form.Group>
					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control type="password" placeholder="Enter your password" value={password1} onChange={event => setPassword1(event.target.value)} required />
					</Form.Group>
					{/*Confirm Password Field*/}
					<Form.Group>
						<Form.Label>Confirm Password: </Form.Label>
						<Form.Control type="password" placeholder="Confirm your password" value={password2} onChange={event => setPassword2(event.target.value)} required />
							{
								isMatched ?
									<span className="text-success">Passwords Matched!</span>
								:
									<span className="text-danger">Passwords Should Match!</span>
							}
					</Form.Group>

					{/*Register Button*/}
					{
						isActive ? 
							<Button className="mb-3 btn-success btn-block" type="submit"> Register </Button>
						:
							<Button className="mb-3 btn-secondary btn-block" disabled> Register </Button>
					}

				</Form>
			</Container>
		</>
	);
};