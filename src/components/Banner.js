// This componenet will be used as the hero section of our page
// Responsive -> grid system of bootstrap

import {Row, Col} from 'react-bootstrap';

// we will use default bootstrap utility classes to format the component


// Create a function that will describe the structure of the hero section

// 'class' -> reserved keyword (HTML)
// React/JSX Elements -> 'className'
export default function Banner({bannerData}) {
	return(
		<Row className="mt-5 p-5">
			<Col className="text-center">
				<h1> {bannerData.title} </h1>
				<p className="my-4"> {bannerData.content} </p>
				<a className="btn btn-success" href="/">Start Learning</a>
			</Col>
		</Row>
	);
}
