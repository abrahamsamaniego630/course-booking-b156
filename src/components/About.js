// Bootstrap Grid System
// Title: About Me
// Full Name: Author
// Job Description: Full Stack Web Developer
// Description: About Yourself
// Contacts: (Email, Mobile No, Address)
import {Row, Col, Container} from 'react-bootstrap';

export default function About() {
	return(
		<Container className="my-3 d-flex justify-content-center">
			<Row className="text-left aboutRow">
				<Col className="p-1 aboutCol" xs={12}>
					<h1>About Me</h1>
					<h4>Abraham Samaniego</h4>
					<h5>Full Stack Web Developer</h5>
					<p>I am career shifter who now wishes to pursue the world of programming through Full Stack Web Development.</p>
				<Col className="p-1 aboutCol" xs={12}>
					<h1>Contact Me</h1>
						<ul>
							<li>Email: abrahamsamaniego630@gmail.com</li>
							<li>Mobile No.: 09171234567</li>
							<li>Address: Malolos, Bulacan</li>
						</ul>
				</Col>	
				</Col>
			</Row>
		</Container>
	)
}