import React from 'react';
// this module was created to provide the necessary information about the user/client who is using the app

// 1. Create the 'context' about the topic
	// createContext() => will allow us to create context 'object' that will allow us to store
	// information about the subject
const UserContext = React.createContext() //container for the information

// 2. acquire the provider property of the newly created context object.
	// Visualize:
	// UserContext = {
	// 	propsAboutTheUser: value
	// 	// Provider:
	// }
	// Provider = > will allow us to use its utility to permit subcomponents of our app to
	// consume/use the available information about out subject. you'll need to expose this utility/data
export const UserProvider = UserContext.Provider;

// 3. Identify the module on your app who will take the mantle/role of the provider.

// Consumers
// Expose the context object for the consumers
export default UserContext;